#!/usr/bin/env python3

import argparse
import logging
import os
import pathlib
import shutil
import subprocess
import tempfile
import time
import urllib
from datetime import datetime
from typing import Sequence, Tuple

from sh import sha256sum

# Use sh.contrib to avoid interactive pager issues
# https://amoffat.github.io/sh/sections/contrib.html#git
from sh.contrib import git

DEFAULT_CS9_REPO = "https://gitlab.com/CentOS/automotive/sample-images.git"


def generate_git(
    repo_dir: pathlib.Path,
    repo: str,
    branch: str,
    lockfile_path: str,
    new_lockfile_path: str,
) -> bool:
    """clones git repo and applies new lockfile"""
    logging.getLogger("sh").setLevel(logging.WARNING)
    git.clone("-b", branch, repo, str(repo_dir))
    logging.getLogger("sh").setLevel(logging.INFO)

    # Copy new lockfile over old lockfile in git repo
    shutil.copyfile(new_lockfile_path, str(repo_dir / lockfile_path))

    # Debug git diff
    diff = git.diff(lockfile_path, _cwd=repo_dir)
    logging.info(diff)

    # Run git porcelain to see if there's a diff
    porcelain = git.status("--porcelain", str(lockfile_path), _cwd=repo_dir)

    # Return True if diff is there, False if no diff
    return bool(porcelain)


def generate_lockfile_branch(
    repo_dir: pathlib.Path, lockfile_path: str
) -> Tuple[str, str]:
    """make branch from lockfile change in git repo"""
    # Get timestamp for commit message / MR message
    timestamp = datetime.utcnow().strftime("%Y-%m-%d %H:%M")

    # Create git commit for lockfile
    if "GIT_AUTHOR_EMAIL" not in os.environ:
        os.environ["GIT_AUTHOR_EMAIL"] = "automotive-a-team@redhat.com"
    if "GIT_COMMITTER_EMAIL" not in os.environ:
        os.environ["GIT_COMMITTER_EMAIL"] = "automotive-a-team@redhat.com"
    if "GIT_AUTHOR_NAME" not in os.environ:
        os.environ["GIT_AUTHOR_NAME"] = "Lockfile Bot"
    if "GIT_COMMITTER_NAME" not in os.environ:
        os.environ["GIT_COMMITTER_NAME"] = "Lockfile Bot"

    lockfile_name = os.path.basename(lockfile_path)
    commit_msg = f"{lockfile_name}: update for {timestamp}"
    git.add(lockfile_path, _cwd=repo_dir)
    git.commit("-a", "-m", commit_msg, _cwd=repo_dir)

    # Calculate checksum of lockfile
    full_lockfile_path = repo_dir / lockfile_path
    checksum = str(sha256sum(str(full_lockfile_path)))[0:8]

    # Create git branch
    branch_name = f"lockfile_update_{checksum}"
    git.checkout("-b", branch_name, _cwd=repo_dir)

    # Return branch name
    return branch_name, commit_msg


def branch_is_in_remote(repo_dir: pathlib.Path, branch: str) -> bool:
    """check if branch exists in remote"""
    # Call git ls-remote to check for the branch
    is_in_remote = git("ls-remote", "--heads", "origin", branch, _cwd=repo_dir)

    return bool(is_in_remote)


def remote_branch_is_up_to_date(
    repo_dir: pathlib.Path, remote_branch: str, main_branch: str
) -> bool:
    """check if branch in remote is rebased off main"""
    # Find where the branch branched off the main branch
    branch_off_point = git(
        "merge-base",
        f"origin/{main_branch}",
        f"origin/{remote_branch}",
        _cwd=repo_dir,
    )

    # Find the current head of the main branch
    current_head = git(
        "show-ref", "-s", f"origin/{main_branch}", _cwd=repo_dir
    )

    # If the current head is equivalent to the branch off point
    # then the branch is up to date
    return branch_off_point == current_head


def create_merge_request(
    repo_dir: pathlib.Path,
    branch_name: str,
    *,
    remote_name: str = "origin",
    set_upstream: bool = True,
    force: bool = True,
    target_branch: str = "main",
    labels: Sequence[str] = None,
    delete_source_branch_on_merge: bool = True,
    merge_when_pipeline_succeeds: bool = True,
    title: str = "Merge Request",
) -> None:
    git_opts = []
    if set_upstream:
        git_opts.append("-u")

    if force:
        git_opts.append("-f")

    git_opts.extend([remote_name, branch_name])

    gitlab_mr_opts = [
        "-o",
        "merge_request.create",
        "-o",
        f"merge_request.target={target_branch}",
        "-o",
        f"merge_request.title={title}",
    ]

    if delete_source_branch_on_merge:
        gitlab_mr_opts.extend(["-o", "merge_request.remove_source_branch"])

    if merge_when_pipeline_succeeds:
        gitlab_mr_opts.extend(
            ["-o", "merge_request.merge_when_pipeline_succeeds"]
        )

    labels = labels or []
    for label in labels:
        gitlab_mr_opts.extend(["-o", f"merge_request.label={label}"])

    git.push(git_opts + gitlab_mr_opts, _cwd=repo_dir)


def make_lockfile_merge_request(
    repo, branch, lockfile_path, new_lockfile_path, dry_run
) -> None:
    """clone the git, update the lockfile, and make a merge request"""
    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpdir = pathlib.Path(tmpdirname)
        repo_dir = tmpdir / "repo"

        logging.info("Cloning git and applying the new lockfile")
        has_diff = generate_git(
            repo_dir, repo, branch, lockfile_path, new_lockfile_path
        )
        if not has_diff:
            logging.info("No change in lockfile")
            return

        logging.info("Creating unique lockfile branch")
        lockfile_branch_name, commit_message = generate_lockfile_branch(
            repo_dir, lockfile_path
        )

        logging.info("Checking remote branch status")
        if branch_is_in_remote(repo_dir, lockfile_branch_name):
            if remote_branch_is_up_to_date(
                repo_dir, lockfile_branch_name, branch
            ):
                logging.info("Branch already exists and is up to date.")
                return
            logging.info("Remote branch exists but is not up to date")
        else:
            logging.info("Branch does not exist in remote")

        logging.info("Creating merge request")
        create_merge_request(
            repo_dir,
            lockfile_branch_name,
            target_branch=branch,
            labels=["automated-updates"],
            title=commit_message,
        )


def main() -> None:
    """main function"""
    parser = argparse.ArgumentParser(description="Lockfile updater script")
    parser.add_argument(
        "--repo",
        type=str,
        help="Lockfile git repository url",
        default=None,
    )
    parser.add_argument(
        "--branch",
        type=str,
        help="Lockfile git repository branch",
        default="main",
    )
    parser.add_argument(
        "--lockfile-path",
        type=str,
        help="Path to lockfile in automotive sig repository",
        default="package_list/cs9-image-manifest.lock.json",
    )
    parser.add_argument(
        "--dry-run", action="store_true", help="Run script without git push"
    )

    parser.add_argument(
        "--kojiserver", type=str, help="Path to koji server", default=None
    )

    parser.add_argument(
        "--kojitag", type=str, help="Koji tag, ex: mytag-1.0.0 ", default=None
    )

    parser.add_argument(
        "--forcenvrs",
        type=str,
        nargs="+",
        help=(
            "NVRs to be forcefully added after"
            "base packages have been resolved"
        ),
        default=None,
    )

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    logging.info("Starting...")

    # Determine repo to clone
    repo = args.repo or os.environ.get("REPO") or DEFAULT_CS9_REPO

    # Debug the repo without exposing any oauth credentials
    safe_repo_str = repo
    if "@" in repo:
        parsed = urllib.parse.urlparse(repo)
        replaced = parsed._replace(
            netloc="{}:{}@{}".format(parsed.username, "???", parsed.hostname)
        )
        safe_repo_str = (
            replaced.geturl()
        )  # 'https://user:???@example.com/path?key=value#hash'

    logging.info("Repository=%s", safe_repo_str)
    logging.info("Lockfile Path=%s", args.lockfile_path)

    # Generic the lockfile using make-lockfile.py
    logging.info("Calling make-lockfile.py")

    # Find a writeable tmp file path to output the new json
    tmpdir = pathlib.Path(tempfile.gettempdir())
    new_lockfile = tmpdir / f"new.{int(time.time())}.json"

    # Call make-lockfile.py
    script_dir = pathlib.Path(os.path.dirname(os.path.realpath(__file__)))
    make_lockfile_script = (
        script_dir.parent / "make-lockfile" / "make-lockfile.py"
    )

    make_lockfile_cmd = [
        "python3",
        str(make_lockfile_script),
        "--outfile",
        str(new_lockfile),
    ]

    # If MAKE_LOCKFILE_DISTRO is defined, we need to forward it to
    # make-lockfile.py as the --distro param
    make_lockfile_distro = os.environ.get("MAKE_LOCKFILE_DISTRO")
    if make_lockfile_distro:
        make_lockfile_cmd.extend(["--distro", make_lockfile_distro])

    # If MAKE_LOCKFILE_REPOS is defined, we need to forward them to
    # make-lockfile.py as the --repos params.
    make_lockfile_repos = os.environ.get("MAKE_LOCKFILE_REPOS")
    if make_lockfile_repos:
        make_lockfile_repos = make_lockfile_repos.split()
        make_lockfile_cmd.extend(["--repos"] + make_lockfile_repos)

    # Check if kojiserver and kojitag parser args are provided
    # Then extend make_lockfile_cmd with --kojiserver and --kojitag
    if args.kojiserver and args.kojitag:
        make_lockfile_cmd.extend(["--kojiserver", args.kojiserver])
        make_lockfile_cmd.extend(["--kojitag", args.kojitag])

    if args.forcenvrs:
        make_lockfile_cmd.extend(["--forcenvrs"] + args.forcenvrs)

    logging.info(" ".join(make_lockfile_cmd))
    subprocess.run(make_lockfile_cmd, check=True)
    logging.debug("Successfully created lockfile: %s", new_lockfile)

    logging.info("Creating MR in git repository")
    make_lockfile_merge_request(
        repo,
        args.branch,
        args.lockfile_path,
        str(new_lockfile),
        args.dry_run,
    )
    logging.info("Done!")


if __name__ == "__main__":
    main()
