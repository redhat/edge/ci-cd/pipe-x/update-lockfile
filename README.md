# update-lockfile

update-lockfile is a utility to generate lockfiles from content-resolver
workloads and make GitLab merge requests to update a lockfile stored in a
GitLab git repository.

## About

This project contains a program named berlin.py which is a wrapper around the
[make-lockfile](https://gitlab.com/redhat/edge/ci-cd/services/make-lockfile/)
program. Berlin.py extends the functionality of make-lockfile to also create
GitLab merge requests that update the lockfile in the
[AutoSD sample-images](https://gitlab.com/CentOS/automotive/sample-images/)
repository.

This project also holds the gitlab-ci.yml configuration to run this project on
a schedule to do the updates.

## HLA Diagram

The following is an HLA of the project (not reflecting actual internals).

![update-lockfile](docs/update-lockfile.png "update-lockfile")

## Contibuting

### Pipleline

`.gitmodules` references the latest
[make-lockfile](https://gitlab.com/redhat/edge/ci-cd/services/make-lockfile/)
which is invoked by `./src/berlin.py` to create the lockfile.

For every MR or MR merge a unique container is built on `$CONTAINER_REGISTRY`
containing the newly committed code. The `./Containerfile` defines the content
of the container. The build stage inherited from
[container-build](https://gitlab.com/redhat/edge/ci-cd/pipe-x/pipelines-as-code/-/blob/main/.gitlab/containers.yml)
defines how where and when the container is built and its meta-data.

The `run` job is invoked on a schedule that is defined by
[rules](https://gitlab.com/redhat/edge/ci-cd/pipe-x/pipelines-as-code/-/blob/main/.gitlab/rules.yml)
and uses the latest merged container on the `main` branch to invoke `berlin.py`.

The lint and unit jobs directly invoke corresponding targets in `./Makefile`
using a standard fedora container.

### pre-commit integration

With **pre-commit** you can test your code for small issues and run the appropriate
linters locally.

> :pushpin: To make use of this feature you will need to [install Pre-commit](#installing-pre-commit)
on your local machine.

To enable this, simply run `pre-commit install` from the project directory at any
stage after cloning and before committing. Now, every `git commit` run will be
followed by the hooks defined in [.pre-commit-config.yaml](.pre-commit-config.yaml).
Unless all tests have passed, the commit will be aborted.

#### Running hooks

The configured hooks will only run against the files that have been modified or added.
To allow testing on all files in the repository instead, you can use the following
command:

```bash
pre-commit run --all-files
```

and to run individual hooks:

```bash
pre-commit run <hook_id>
```

> :pushpin: Some hooks have local dependencies (e.g. markdownlint requires RubyGems)

If you wish to perform pre-commit testing but want to skip any specific test/hook
use SKIP on commit.
The SKIP environment variable is a comma separated list of hook ids as defined
in [.pre-commit-config.yaml](.pre-commit-config.yaml)

```bash
SKIP=<hook_id>, <hook_id> git commit -m "foo"
```

If you decide not to use pre-commit after enablement, `pre-commit uninstall` will
restore your hooks to the state prior to installation.
Alternatively, you can run your commit with `--no-verify`.

#### Installing Pre-commit

To install the pre-commit package manager, run the respective command for your preferred
package manager:

```bash
pip install pre-commit
```

```bash
brew install pre-commit
```

```bash
conda install -c conda-forge pre-commit
```
