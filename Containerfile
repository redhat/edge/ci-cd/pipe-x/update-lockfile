FROM registry.access.redhat.com/ubi8/python-39:1-67

USER root

RUN curl -sSL https://install.python-poetry.org | python3 -

RUN mkdir -p /berlin
WORKDIR /berlin

COPY . /berlin

# Poetry needs to be fed some manual information to know that there is a
# virtualenv environment wrapper in the container (due to s2i-python-container).
# If we don't obey this virtualenv, poetry will install to a system site-packages
# directory which is not picked up automatically by the container python. Fixing
# this results in modules being installed correctly into /opt/app-root.
#
# References:
# https://github.com/sclorg/s2i-python-container/issues/466
# https://pythonspeed.com/articles/activate-virtualenv-dockerfile/
ENV VIRTUAL_ENV=/opt/app-root

RUN poetry config virtualenvs.create false && \
    poetry install && \
    rm -rf $HOME/.cache

ENTRYPOINT [ "/bin/sh" ]
